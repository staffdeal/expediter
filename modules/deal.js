var Moment = require('moment');

function deal (doc) {

    if (doc.provider && doc.id && doc.title && doc.startDate && doc.endDate) {
        this.id = doc.id;
        this.provider = doc.provider;
        this.title = doc.title;

        this.startDate = Moment(doc.startDate);
        this.endDate = Moment(doc.endDate);

        this.teaser = '';
        if (doc.teaser) {
            this.teaser = doc.teaser;
        }

        this.text = '';
        if (doc.text) {
            this.text = doc.text;
        }

        this.titleImage = '';
        if (doc.titleImage) {
            this.titleImage = doc.titleImage;
        }

        this.featured = false;
        if (doc.featured) {
            this.featured = doc.featured;
        }

        this.pass = {
            online: {},
            onsite: {}
        };
        if (doc.pass) {
            if (doc.pass.online) {

            }
            if (doc.pass.onsite) {
                var onsite = doc.pass.onsite;
                if (onsite.barcode) {
                    this.pass.onsite.barcode = onsite.barcode;
                }
                if (onsite.type) {
                    this.pass.onsite.type = onsite.type;
                }
                if (onsite.prerendered) {
                    this.pass.onsite.prerendered = onsite.prerendered;
                }
            }
        }



    } else {
        return null;
    }


}

module.exports = deal;