

function expediter() {

    var config = require('config');
    this.privateKey = config.security.key;

}

function sharedInstance() {
    if (sharedExpediter === null) {
        sharedExpediter = new expediter();
    }
    return sharedExpediter;
}

var sharedExpediter = null;
module.exports = sharedInstance();