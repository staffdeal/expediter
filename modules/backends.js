backend = require('./backend');

var sharedBackends = null;

function Backends() {

    this.backends = new Array();

    this.load = function(callback) {

        logger.info('Loading backend data');

        var backendsStore = this.backends;

        db.view('backends','byId', function (error, response) {
            if (!error) {
                response.rows.forEach(function (doc) {

                    var name = doc.value.name;
                    var url = doc.value.url;
                    if (name && url) {
                        var loadedBackend = new backend(url, name);
                        loadedBackend.id = doc.id;

                        loadedBackend.active = (doc.value.active);

                        backendsStore.push(loadedBackend);
                    }

                });
                logger.info('Loaded ' + backendsStore.length + ' backend sets');
            } else {
                logger.critical('Could not load backend data, view did return error');
            }

            callback(backendsStore);
        });

    }

    this.count = function() {
        return this.backends.length;
    }

    this.allBackends = function() {
        return this.backends;
    }

    this.backendById = function (id) {

        var backend = null;

        this.backends.forEach(function (item) {
           if (item.id == id) {
               backend = item;
               return;

           }
        });

        return backend;
    }

}

function sharedInstance() {
    if (sharedBackends == null) {
        sharedBackends = new Backends();
    }
    return sharedBackends;
}

module.exports = sharedInstance();
