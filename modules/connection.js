var EventEmitter = require('events').EventEmitter;
var Util = require('util');

var S = require('string');
var Request = require('request');
var Message = require('./message');

function connection(url, privateKey) {

    this.url = S(url).chompRight('/').ensureRight('/api/com').s;
    this.privateKey = privateKey;

    this.remoteKey = null;

    this.send = function (message, callback) {

        this.emit('start', message);

        message.key = this.privateKey;

        var options = {
            uri: this.url,
            json: {
                message: message
            }
        }

        //options.uri = options.uri + '?start_debug=1&send_sess_end=1&debug_start_session=1&debug_session_id=17991&debug_port=10137&debug_host=192.168.208.24%2C127.0.0.1';

        var that = this;

        logger.info('Starting Message Request to ' + this.url);
        Request(options, function (error, response, body) {

            if (!error) {

                var statusCode = response.statusCode;
                if (statusCode == 200) {
                    logger.info('Received response 200 from ' + that.url);
                } else {
                    logger.error('Received response ' + statusCode + ' from ' + that.url);
                }

                var signedCorrectly = true;
                if (that.remoteKey) {
                    signedCorrectly = false;
                    if (body && body.message && body.message.key) {
                        if (body.message.key == that.remoteKey) {
                            signedCorrectly = true;
                        } else {
                            error = 'Message had wrong signing key.';
                        }
                    }
                }

                if (signedCorrectly && body && body.message && body.message.name) {

                    var message = new  Message(body.message.name);
                    if (message.importResponse(body.message)) {
                        if (message.value('status') != 'Error') {
                            body = message;
                        } else {
                            var error = (message.value('Description')) ? message.value('Description') : 'Unspecified error';
                        }
                    } else {
                        error = 'Response could not be parsed';
                    }

                } else {
                   error = 'Response did not contain a valid message object';
                }
            }

            if (error) {
                body = null;
                logger.critical('Error while processing Message Response: ' + error);
                this.emit('error', error);
            } else {
                this.emit('finished', body);
            }

            if (callback) {
                callback(error, body);
            }
        });
    };
}

Util.inherits(connection, EventEmitter);
module.exports = connection;