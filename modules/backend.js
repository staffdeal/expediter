var EventEmitter = require('events').EventEmitter;
var Util = require('util');

var Request = require('request');
var S = require('string');
var URL = require('url');

var Deal = require('./deal');
var Expediter = require('./expediter');
var Connection = require('./connection');
var Message = require('./message');

function backend(url, name) {

    this.id = null;
    this.url = url;
    this.name = name;
    this.active = false;

    this.connection = new Connection(this.url, Expediter.privateKey);

    this.isOnline = false;
    this.isPinging = false;
    this.caption = null;
    this.hadError = false;

    this.needsSync = false;
    this.isSyncing = false;

    this.deals = new Array();
    this.key = null;

    this.startup = function (callback) {

        if (this.active != true) {
            logger.warn('Backend "' + this.name + '" is not active, skipping start up');
            if (callback) {
                callback(false);
            }
            return;
        }

        var that = this;

        var startupData = {
            ping: false,
            deals: false,
            dealers: false
        };

        var startupCallbacksChecker = function () {

            if (startupData.ping && startupData.deals) {
                success = that.isOnline;
                if (success) {
                    logger.info('Backend "' + that.name + '" started successfully');
                } else {
                    logger.error('Backend "' + that.name + '" could not be started');
                }
                if (callback) {
                    callback(success);
                }

            }

        };

        logger.info('Starting up backend "' + this.name + '"');
        this.ping(function (success) {
            startupData.ping = true;

            if (success) {
                setTimeout(function () {
                    that.syncDeals(
                        function (sucess) {
                            startupData.deals = true;
                            startupCallbacksChecker();
                        })
                }, 1000);
            } else {
                startupData.deals = true;
            }
            startupCallbacksChecker();
        });
    }

    this.ping = function (callback) {

        this.isPinging = true;
        var that = this;

        logger.info('Pinging "' + this.name + '"');

        var message = new Message('dispatcher.ping');
        message.addValue('http://heise.de','url');

        this.connection.send(message, function (error, response) {

            that.isOnline = false;
            that.hadError = true;

            if (!error) {
                if (response.value('status') == 'OK' && response.value('caption')) {
                    that.caption = response.value('caption');
                    that.key = response.key;
                    that.connection.remoteKey = that.key;

                    that.isOnline = true;
                    that.hadError = false;
                }
            }

            that.isPinging = false;

            if (callback) {
                callback(!that.hadError);
            }
        });
    }

    this.sync = function (callback) {
        this.isSyncing = true;
        var that = this;

        // first we sync the deals
        this.syncDeals(function (success) {

            that.isSyncing = false;

            if (success) {

            } else {

            }

            that.emit('syncFinished', success);

            if (callback) {
                callback(success);
            }
        });
    }

    this.syncDeals = function (callback) {

        logger.info('Start deal sync of "' + this.name + '"');
        var that = this;

        var requestMessage = new Message('dispatcher.deals');
        this.connection.send(requestMessage, function (error, response) {

            if (!error) {
                var formatError = true;
                var numberOfLoadedDeals = 0;
                var numberOfNewDeals = 0;
                if (response.value('deals')) {
                    var loadedDealsData = response.value('deals');
                    if (Array.isArray(loadedDealsData)) {

                        var anyLoadError = false;
                        loadedDealsData.forEach(function (rawDeal) {
                            if (rawDeal.id && rawDeal.title && rawDeal.startDate && rawDeal.endDate) {
                                var newDeal = new Deal(rawDeal);
                                if (newDeal) {
                                    numberOfLoadedDeals++;
                                    if (that.getDealWithId(newDeal.id) == null) {
                                        numberOfNewDeals++;
                                    }
                                    that.addDeal(newDeal);
                                } else {
                                    anyLoadError = anyLoadError || true;
                                }
                            } else {
                                anyLoadError = anyLoadError || true;
                            }
                        });

                        if (anyLoadError == false) {
                            formatError = false;
                        }
                    }
                }

                if (formatError) {
                    error = 'Response did not contain any deals data';
                    logger.critical(error);
                } else {
                    logger.info('Loaded ' + numberOfLoadedDeals + ' deals, ' + numberOfNewDeals + ' new, ' + that.deals.length + ' deals total');
                }
            }

            if (callback) {
                callback(error == null);
            }
        });
    }

    this.syncDealers = function (callback) {

        if (callback) {
            callback(true);
        }
    }


    this.getDealWithId = function (id) {

        var hit = null;

        this.deals.forEach(function (candidate) {

            if (candidate.id && candidate.id == id) {
                hit = candidate;
                return;
            }

        });

        return hit;

    }

    this.addDeal = function (deal) {

        if (deal.id) {

            var existingDeal = this.getDealWithId(deal.id);
            if (existingDeal != null) {
                this.deals.removeItem(existingDeal);
            }
            this.deals.push(deal);
        }
    }
};

Util.inherits(backend, EventEmitter);

module.exports = backend;