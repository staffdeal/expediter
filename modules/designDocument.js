function designDocument(name, map) {

    this.name = name;
    this.map = map;
    this.reduce = null;

    this.getName = function() {
        return this.name;
    }

    this.setName = function (name) {
        this.name = name;
        return this;
    }

    this.getMap = function () {
        return this.map;
    }

    this.setMap = function (map) {
        this.map = map;
        return this;
    }

    this.getReduce = function () {
        return this.reduce;
    }

    this.setReduce = function (reduce) {
        this.reduce = reduce;
        return this;
    }

};

module.exports = designDocument;