function message (name) {

    this.name = name;

    this.values = {};

    this.key = null;

    this.importResponse = function (response) {
        if (response.name == this.name) {

            if (response.values) {
                this.values = response.values;
            }

            if (response.key) {
                this.key = response.key;
            }

            return true;
        }
        return false;
    }

    this.addValue = function (value, key) {
        this.values[key] = value;
    }

    this.value = function (key) {
        return (this.values[key]) ? this.values[key] : null;
    }

}

module.exports = message;