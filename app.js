/**
 * Tools
 */
var tools = require('./tools.js');


/**
 * Configuration
 */

var config = require('config');


/**
 * Airbrake setup
 */
var airbrakeEnabled = false;
var airbrakeConfig = config.airbrake;
if (airbrakeConfig) {
    var enabled = airbrakeConfig.enabled || false;
    var key = airbrakeConfig.key || false;

    if (enabled && key) {
        var airbrake = require('airbrake').createClient(key);
        airbrake.serviceHost = "exceptions.codebasehq.com";
        airbrake.protocol = "https";
        airbrakeEnabled = true;
        airbrake.handleExceptions();
    }
}

// Preparing the logger
logger = require('levellogger').logAll();

/**
 * Module dependencies.
 */

var express = require('express')
    , routes = require('./routes')
    , http = require('http')
    , path = require('path')
    , backends = require('./modules/backends')
    , backend = require('./modules/backend')
    , expediter = require('./modules/expediter');

var app = express();

app.configure(function () {
    app.set('port', process.env.PORT || 3000);
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.cookieParser('your secret here'));
    app.use(express.session());
    app.use(app.router);
    app.use(require('stylus').middleware(__dirname + '/public'));
    app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function () {
    app.use(express.errorHandler());
});

app.get('/', routes.index);

var backendsRoute = require('./routes/backends');
app.get('/backends', backendsRoute.list);
app.get('/backends/list', backendsRoute.list);
app.get('/backends/ping/:id', backendsRoute.ping);
app.get('/backends/sync/:id', backendsRoute.sync);

var dealsRoute = require('./routes/deals');
app.get('/deals/:backendId', dealsRoute.list);

var apiRoute = require('./routes/api');
app.get('/api/deals', apiRoute.deals);

/* Database setup */
db = null;
var dbConfig = config.database;
if (dbConfig) {

    tools.setupDatabase(dbConfig.host, dbConfig.name, dbConfig.port, function(err, preparedDb) {
        "use strict";

        if (preparedDb) {

            db = preparedDb;

            var backendDesigns = require('./design/backends');
            tools.createDesignDocuments('backends', backendDesigns, function() {

                var expediterInstance = expediter;

                backends.load(function (loadedBackends) {

                    loadedBackends.forEach(function (item) {
                       item.startup();
                    });

                    http.createServer(app).listen(app.get('port'), app.get('ip'), function () {
                        logger.info("Webserver ready on port " + app.get('port'));
                    });

                });

            })

        } else {
            logger.critical('Could not connect to database. Webserver NOT started.');
        }
    });

} else {
    logger.critical('Could not load DB config - stoping…');
}





