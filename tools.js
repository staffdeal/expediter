var dbObject;

module.exports = {

    setupDatabase: function (dbHost, dbName, dbPort, callback) {
        // Setting up the database
        "use strict";

        var host = dbHost || false;
        var name = dbName || false;
        var port = dbPort || 5984;

        if (host && name && port > 0) {

            var dbConnectionString = 'http://' + host + ':' + port;
            logger.info('Database connection string: ' + dbConnectionString);

            var nano = require('nano')(dbConnectionString);

            nano.db.list(function (err, body) {

                var hasDatabase = false;
                body.forEach(function (dbName) {
                    if (dbName === name) {
                        hasDatabase = true;
                    }
                });

                if (hasDatabase) {
                    logger.info('Database ' + name + ' already exists');
                    dbObject = nano.use(name);
                    callback(null, dbObject);
                } else {
                    logger.info('Creating database ' + name);
                    nano.db.create(name, function (err, body) {
                        if (err) {
                            logger.error(err);
                        }
                        dbObject = nano.use(name);
                        callback(err, dbObject);
                    });
                }
            });

            var createDesignDocuments = function () {


                dbObject.insert( {
                    "views": {
                        "by_id": {
                            "map": function(doc) {
                                if (doc.type == 'backend') {
                                    emit([doc._id, doc], doc._id);
                                }
                            }
                        }
                    }
                }, '_design/backends', function (error, response) {
                        console.log("yay");
                    });


            };
        }
    },

    createDesignDocuments : function (documentName, views, callback) {

        if (Array.isArray(views)) {
            logger.info('Creating design document "' + documentName + '" containing ' + views.length + ' views');

            var data = {};

            views.forEach(function (view) {
                var viewName = view.getName();
                var viewMap = view.getMap();
                var viewReduce = view.getReduce();

                if (viewName && viewMap) {
                    var viewData = {
                        'map' : viewMap
                    };

                    if (viewReduce) {
                        viewData['reduce'] = viewReduce;
                    }

                    data[viewName] = viewData;
                }
            });

            var dbDocument = {
                "views" : data
            };

            var designDocumentName = '_design/' + documentName;

            dbObject.get(designDocumentName, function (error, response) {

                if (response) {
                    logger.info('Design document "' + designDocumentName + '" already exists, trying to update.');
                    var _rev = response._rev;
                    dbDocument['_rev'] = _rev;
                }

                dbObject.insert(dbDocument, designDocumentName, function (error, response) {
                    callback(error);
                });
            })

        } else {
            callback();
        }

    }
}

// Array Remove - By John Resig (MIT Licensed)
Array.prototype.remove = function(from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
};

Array.prototype.removeItem = function (item) {

    if (item) {
        var itemIndex = this.indexOf(item);
        if (itemIndex >= 0) {
            return this.remove(itemIndex, itemIndex);
        }
    }
    return this;
}