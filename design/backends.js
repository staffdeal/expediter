var designs = null;
var DesignDocument = require('../modules/designDocument');


if (designs == null) {

    designs = new Array();

    designs.push(new DesignDocument('byId', function (doc) {
        if (doc.type == 'backend') {
            emit(doc._id, doc);
        }
    }));

    designs.push(new DesignDocument('byName', function (doc) {
        if (doc.type == 'backend') {
            emit(doc.name, doc);
        }
    }));

    designs.push(new DesignDocument('byUrl', function (doc) {
        if (doc.type == 'backend') {
            emit(doc.url, doc);
        }
    }));


}


module.exports = designs;
