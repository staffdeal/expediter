var Backends = require('../modules/backends');

exports.list = function (req, res) {

    var success = false;

    var backendId = req.params.backendId;
    if (backendId) {
        var backend = Backends.backendById(backendId);

        res.render('deals/list', {
            title: 'Deals ' + backend.name,
            deals: backend.deals
        });

        success = true;

    }

    if (success == false) {
        res.render('deals/list', {
            title: 'Deals'
        });
    }
};