var backendCollection = require('../modules/backends');

exports.list = function (req, res) {
    res.render('backends/list', {
        title: 'Backends List',
        backends : backendCollection.allBackends()
    });
};

exports.ping = function (req, res) {

    var id = req.params.id;
    if (id) {

        var backend = backendCollection.backendById(id);
        if (backend) {
            backend.ping();
        }
    }

    res.redirect('/backends');
}

exports.sync = function (req, res) {

    var id = req.params.id;
    if (id) {

        var backend = backendCollection.backendById(id);
        if (backend) {
            backend.sync();
        }
    }

    res.redirect('/backends');
}